<#
.SYNOPSIS
    Run the experiments described in the thesis. Caution: it takes a long while.
.DESCRIPTION
    This script runs the experiments as specified in the thesis. Afterwards, the app/build/install/app directory contains
    the results of the experiments in the form of folders named '<dataset>_<n>'. These folders must be copied into an empty directory. Then, using the readlogs command of this program, are the log_*.txt files converted into a single 'result.tsv' file.
#>
#Requires -Version 6.0

. .\scripts\commonVariables.ps1

# First, generate the tests
#& .\generateRandom.ps1

# After generating, run the tests.
& .\runFromDirectory.ps1 -forceRunDir $randomtestsdir