<#
.SYNOPSIS
    Generate data sets with random distributions of data.
.DESCRIPTION
    This script builds the software, cd's into the directory it installs 
    itself in and generates the necessary datasets. Without parameters it 
    generates for $datasets up to the n specified in $sizes, each $sampleSize 
    data sets. The software is installed in 
    <directory of run.ps1>\app\build\install\app\ . That is also the directory
    that contains the generated datasets.
.NOTES
    Previous results are overidden!
.PARAMETER datasets 
    The data sets for which to generate random distributions.
.PARAMETER sampleSize
    How much sets of data should be generated per type of data set and size of 
    it.
.PARAMETER forcesizes
    If set, then for all data sets this set of sizes will be said.
.PARAMETER generateIntoDir
    If set, then the data sets are generated into this directory.
.EXAMPLE
    .\generateRandom.ps1
    Generate data sets for floating, teamdefeat and ambiguity. Each up to the
     n specified in
.EXAMPLE
    .\generateRandom.ps1 -datasets @("floating") -sampleSize 30
    Generate 30 test cases per combination of "floating" and 1..20
#>
param(
    [String[]]$datasets = @("teamdefeat", "floating", "ambiguity"),
    [Int32]$sampleSize = 30,
    [switch]$noInstallDistOrDirRemoval,
    [Int32[]]$forceSizes = @(),
    [String]$generateIntoDir = ""
)

$sizes = @{
    "ambiguity" = @(1..12);
    "teamdefeat" = @(1..3);
    "floating" = @(1..15);
}

#Requires -Version 6.0

. .\scripts\commonVariables.ps1

function getSizes {
    param (
        [String]$datasetname = ""
    )
    if ($forceSizes.Length -gt 0) {
        return $forceSizes
    } else {
        return $sizes[$datasetname]
    }
}

function getRandomTestsDir {
    param (
        [Parameter(Mandatory = $true)]
        [String]$dataset,
        [Parameter(Mandatory = $true)]
        [Int32]$n,
        [Parameter(Mandatory = $true)]
        [Int32]$run
    )

    if ($generateIntoDir -ne "") {
        $datasetdir = $generateIntoDir
    } else {
        $datasetdir = $randomtestsdir
    }

    return Join-Path $datasetdir "$dataset`_$n`_$num"
}

if (-not $noInstallDistOrDirRemoval) {
    doInstallDist
}

Set-Location $distdir

# Generate everything
foreach ($dataset in $datasets) {
    $ns = getSizes $dataset
    
    foreach ($n in $ns) {
        foreach ($num in 1..$sampleSize) {
            $Host.UI.RawUI.WindowTitle = "Generating $dataset with size $n for run $num"

            # Generate a random dataset for the $dataset with size $n
            $datasetdir = getRandomTestsDir $dataset $n $num
        
            $cmdline = @("generate", "--datasetdivider", "random", $dataset, $n, $datasetdir)
            
            & $appbindir $cmdline
        }
    }
    
}

Set-Location $oldpwd
