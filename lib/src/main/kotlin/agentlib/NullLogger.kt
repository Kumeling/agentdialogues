/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib
// Package declaration
// (almost) empty line
/***
 * Finally, a new technical marvel! With all the advanced technology around, finding and keeping stuff simple is a challenge.
 * But behold! We have done it, we succeeded in making something so simple, it will be like it does nothing at all. It
 * requires no maintenance, minimal downtime and all of that for a small nominal fee. Join the revolution! Keep software
 * simple!
 *
 * i.e. By design, this does nothing.
 * Some would say that this class is overkill.
 * Some would say that it is a waste of time.
 * BUT, sometimes, the simpler, the better.
 * That is why this class is such a piece of art.
 *
 *
 * Logs to dev/null, or the closest equivalent on each platform. (i.e. it does nothing by design)
 *
 * Unfortunately, this class is no longer a piece of art.
 */
class NullLogger : ILogger {
    override fun ToStdIn(text: String) { // NULL
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    } // NULL
    // Almost empty line
    override fun getStdIn(): String { // NULL
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    } // Class signature
    override fun Log(id: AgentId, level: LogLevel, message: String, loggingEnabled: Boolean, currentLogLevel: LogLevel) { // Method signature
        // Do nothing
    } // END of method
} // END of class
// Useless (empty) line