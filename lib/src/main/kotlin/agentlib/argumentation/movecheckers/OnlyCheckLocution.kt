/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation.movecheckers

import agentlib.argumentation.ILocution
import agentlib.argumentation.IMove

import java.util.Objects

class OnlyCheckLocution<T : ILocution> : MoveEqualsChecker<T> {
    override fun moveEquals(one: IMove<T>, other: IMove<T>): Boolean {
        if (one === other) return true
        if (other.javaClass != one.javaClass) return false
        return if (one.propositions.size != other.propositions.size) false else one.toString() == other.toString()

        // Horrible, horrible hack. compare the string representation
        // since a DelpArgument for some reason can't just check equality in a normal
        // way.
        // Assume that moves implement a prettyprint tostring.
    }
}
