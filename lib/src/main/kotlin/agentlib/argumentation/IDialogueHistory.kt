/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//
// Translated by CS2J (http://www.cs2j.com): 5-2-2018 14:37:45
//

package agentlib.argumentation

/**
 * Interface to represent the history of a dialogue
 */
interface IDialogueHistory<T : ILocution> {

    val length: Int

    val currentDialogueType: String
    fun addMove(move: IMove<T>)

    /**
     * Open a sub dialogue of the specified type.
     * @param copyTopic Whether, when opening a new sub dialogue, the old topic should be copied over
     * @param type The type of the dialogue to start.
     */
    fun openSubdialogue(type: String, copyTopic: Boolean = false)

    fun closeCurrentDialogue()

    fun reset()
}


