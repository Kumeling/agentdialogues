/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation.movehistory

import agentlib.argumentation.*
import net.sf.tweety.commons.BeliefBase
import net.sf.tweety.commons.Formula

import java.util.Arrays

abstract class MoveHistoryTemplate<T : ILocution, TEnum : Enum<TEnum>, T2 : BeliefBase, T3 : Formula>(private val applicableForRoles: Array<TEnum>) : IDialogueTemplate<T, TEnum, T2> {

    protected abstract fun isApplicableInternal(history: MoveHistory<T, T3>, hisTurn: ArgumentationAgent<T, TEnum, T2>, addressedTo: ArgumentationAgent<T, TEnum, T2>, roleOfTurnTaker: TEnum): Boolean

    protected abstract fun getMovesInternal(history: MoveHistory<T, T3>, hisTurn: ArgumentationAgent<T, TEnum, T2>, addressedTo: ArgumentationAgent<T, TEnum, T2>, roleOfTurnTaker: TEnum): List<IMove<T>>

    protected abstract fun applyInternal(history: MoveHistory<T, T3>, hisTurn: ArgumentationAgent<T, TEnum, T2>, moveToApply: IMove<T>)

    override fun isApplicable(history: IDialogueHistory<T>, hisTurn: ArgumentationAgent<T, TEnum, T2>, addressTo: ArgumentationAgent<T, TEnum, T2>, roleOfTurnTaker: TEnum): Boolean {
        assert(history is MoveHistory<*,*>)

        return Arrays.stream(applicableForRoles).anyMatch { tEnum -> tEnum === roleOfTurnTaker } && isApplicableInternal(history as MoveHistory<T, T3>, hisTurn, addressTo, roleOfTurnTaker)
    }

    override fun getMatchingMoves(history: IDialogueHistory<T>, hisTurn: ArgumentationAgent<T, TEnum, T2>, addressTo: ArgumentationAgent<T, TEnum, T2>, roleOfTurnTaker: TEnum): List<IMove<T>> {
        assert(history is MoveHistory<*, *>)

        return getMovesInternal(history as MoveHistory<T, T3>, hisTurn, addressTo, roleOfTurnTaker)
    }

    override fun apply(history: IDialogueHistory<T>, hisTurn: ArgumentationAgent<T, TEnum, T2>, moveToApply: IMove<T>) {
        assert(history is MoveHistory<*,*>)

        applyInternal(history as MoveHistory<T, T3>, hisTurn, moveToApply)
    }
}
