/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentlib.argumentation

import net.sf.tweety.commons.BeliefBase

class StrictTurnTaking<T : ILocution, T2 : Enum<T2>, T3 : BeliefBase>(argumentationAgents: MutableList<ArgumentationAgent<T, T2, T3>>) : TurnTakingRule<T, T2, T3>(argumentationAgents) {
    override lateinit var currentAgent: ArgumentationAgent<T, T2, T3>

    private var _prevAgent = NotInitialised

    override fun findNextAgent() {
        if (_prevAgent == NotInitialised) {
            _prevAgent = agents.indexOf(firstAgent)
        } else if (_prevAgent < 0) {
            _prevAgent = 0
        } else {
            if (_prevAgent + 1 == agents.size) {
                _prevAgent = 0
            } else {
                _prevAgent += 1
            }
        }

        currentAgent = agents[_prevAgent]
    }

    override fun reset() {
        super.reset()

        _prevAgent = NotInitialised
    }

    override fun AddAgent(agent: ArgumentationAgent<T, T2, T3>) {
        agents.add(agent)
    }

    companion object {
        private const val NotInitialised: Int = Int.MIN_VALUE
    }

}
