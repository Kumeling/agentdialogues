<#
.SYNOPSIS
    Generate 'normal' tests. Those are tests in which the first player knows 
    all the rules and the second player the facts.
.NOTES
    If $noInstallDistOrDirRemoval is NOT given, previous results are overwritten.
.PARAMETER nameOfDataset
    The name of the dataset to run.
.PARAMETER sizesTo
    The sizes the dataset should be run on.
.PARAMETER noInstallDistOrDirRemoval
    Whether to avoid running 'gradle installDist' and to avoid removing the install directory.
.PARAMETER generateIntoThisDir
    If specified, generate the data sets into this directory.
.EXAMPLE
    .\run.ps1 -nameOfDataset "teamdefeat" -sizesTo @(2,3)
    Generate the team defeat data set for n=2 and n=3.
#>
param(
    [Parameter(Mandatory=$true)]
    [ValidateSet("ambiguity","floating","teamdefeat")]
    [String]$nameOfDataset,
    [Parameter(Mandatory=$true)]
    [uint32[]]$sizesTo,
    [switch]$noInstallDistOrDirRemoval,
    [String]$generateIntoThisDir = ""
)

#Requires -Version 6.0

. .\scripts\commonVariables.ps1

if (-not $noInstallDistOrDirRemoval) {
    doInstallDist
}

if ($generateIntoThisDir -ne "") {
    $normaltestsdir = $generateIntoThisDir
}

Set-Location "$distdir"

foreach ($size in $sizesTo) {
    $Host.UI.RawUI.WindowTitle = "Generating $nameOfDataset with size $size"

    $datasetdir = Join-Path $normaltestsdir -ChildPath "\$nameOfDataset`_$size"
    & $appbindir generate $nameOfDataset $size $datasetdir
} 


Set-Location $oldpwd