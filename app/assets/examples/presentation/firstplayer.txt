CanFly -< IsBird.
~CanFly -< IsInjured.
~CanFly -< IsPenguin.
~IsPenguin -< IsPuffin.
TRUE.