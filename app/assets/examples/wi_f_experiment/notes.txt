Created to investigate whether an exhaustive strategy that is only limited
in argument inquiry dialogues (assert no argument for any conclusion that
already got one), leads to an un-complete dialogue.

The answer is no, as any argument not made in such an AI, is in fact asserted
in the WI dialogue.