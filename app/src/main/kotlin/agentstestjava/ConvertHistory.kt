package agentstestjava

import java.lang.IllegalStateException
import java.util.*

enum class DialogueType(val type: String) {
    Ai("ai"), Wi("wi")
}

fun Int.clamp(min: Int, max: Int) =
        when {
            this > max -> max
            this < min -> min
            else -> this
        }


/**
 * Converts a history that was output from this program,
 * into a format that is understood by LaTeX. This includes
 * indenting for sub-dialogues.
 */
fun convertHistory(history: List<String>): List<String> {
    val currentDialogueStack = Stack<DialogueType>()

    val convertedMoves = mutableListOf<String>()

    var numCloses = 0

    convertedMoves += "\\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]"

    var i = 1

    for (move in history) {
        if (move.isEmpty()) {
            continue
        }

        val match = regex.matchEntire(move) ?: throw IllegalStateException("Invalid move.")
        val player = match.groupValues[1]
        val realMoveName = match.groupValues[2]
        val contentsOfMove = match.groupValues[3]
                .trim()
                .replace("{","\\{")
                .replace("}","\\}")

        val prefixToAdd = when (realMoveName.toLowerCase()) {
            "openaiinwi" -> "ai,"
            "openaiinai" -> "ai,"
            "wiopen" -> "wi,"
            else -> ""
        }

        val convertedContents = "$prefixToAdd \\text{\\texttt{$contentsOfMove}}"

        val currentMoveIsOpenMove = convertMoveName(realMoveName) == "open"

        if (currentMoveIsOpenMove) {
            val typeToPush = when (realMoveName) {
                "WiOpen" -> DialogueType.Wi
                else -> DialogueType.Ai
            }

            currentDialogueStack.push(typeToPush)
        }

        val currentSubDialogueType =
                if (currentMoveIsOpenMove) {
                    currentDialogueStack[(currentDialogueStack.size - 2).clamp(0, Int.MAX_VALUE)]
                } else {
                    currentDialogueStack.peek()
                }

        val currentSubDialogueLevel =
                if (currentMoveIsOpenMove) {
                    (currentDialogueStack.size - 2).clamp(0, Int.MAX_VALUE)
                } else {
                    currentDialogueStack.size - 1
                }

        val convertedMove = "\\item[$i.]" +
                "\\hspace{2em}".repeat(currentSubDialogueLevel) +
                "\\ensuremath{\\langle ${convertPlayer(player)}, " +
                "${currentSubDialogueType.type}, " +
                "${convertMoveName(realMoveName)}($convertedContents)" +
                "\\rangle}"

        if (realMoveName == "Close") {
            numCloses += 1

            if (numCloses == 2) {
                currentDialogueStack.pop()
                numCloses = 0
            }
        } else {
            numCloses = 0
        }


        i += 1
        convertedMoves.add(convertedMove)
    }

    convertedMoves += "\\end{enumerate}"

    return convertedMoves
}

val regex = """^(\w+): (\w+) \[(.+)]$""".toRegex()

fun convertMoveName(name: String): String =
        when (name.toLowerCase()) {
            "close" -> "close"
            "assert" -> "assert"
            "wiopen" -> "open"
            "openaiinwi" -> "open"
            "openaiinai" -> "open"
            else -> throw IllegalStateException("Not known move name")
        }

fun convertPlayer(player: String): String =
        when (player) {
            "Opponent" -> "O"
            "Proponent" -> "P"
            else -> throw IllegalStateException("Player unknown")
        }