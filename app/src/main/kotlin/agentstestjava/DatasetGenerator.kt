/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava

import java.util.*
import kotlin.math.pow
import kotlin.random.Random

object DatasetGenerator {
    data class CompletedDataset(val firstPlayer: List<String>, val secondPlayer: List<String>, val levels: List<String>, val initial: String)

    data class UnfinishedDataset(val entries: List<DatasetEntry>, val initial: String)

    data class DividedDataset(val firstPlayer: List<DatasetEntry>, val secondPlayer: List<DatasetEntry>, val initial: String)

    data class DatasetEntry(val proposition: String, val level: Int)

    interface DatasetDivider {
        fun divideDatasetEntries(unfinished: UnfinishedDataset): DividedDataset
    }

    fun DatasetEntry.entryToString(includeLevel: Boolean): String {
        var temp = ""

        temp += "$proposition."

        if (includeLevel) {
            temp += "; $level"
        }

        return temp
    }

    class RandomDivider : DatasetDivider {

        override fun divideDatasetEntries(unfinished: UnfinishedDataset): DividedDataset {
            val first = mutableListOf<DatasetEntry>()
            val second = mutableListOf<DatasetEntry>()

            for (entry in unfinished.entries) {
                if (Random.nextBoolean()) {
                    first += entry
                } else {
                    second += entry
                }
            }

            return DividedDataset(first, second, unfinished.initial)
        }
    }

    infix fun String.withLevel(level: Int): DatasetEntry {
        return DatasetEntry(this, level)
    }

    fun String.asFact(): String = "$this -< TRUE"

    class FirstPlayerFactsDivider : DatasetDivider {
        override fun divideDatasetEntries(unfinished: UnfinishedDataset): DividedDataset {
            val first = mutableListOf<DatasetEntry>()
            val second = mutableListOf<DatasetEntry>()

            for (entry in unfinished.entries) {

                if ("TRUE" in entry.proposition) {
                    second += entry
                } else {
                    first += entry
                }
            }

            return DividedDataset(first, second, unfinished.initial)
        }

    }

    fun dividedDatasetToDataset(divided: DividedDataset) : CompletedDataset {
        val fixedFirstPlayer = divided.firstPlayer + DatasetEntry("TRUE", 1)
        val fixedSecondPlayer = divided.secondPlayer + DatasetEntry("TRUE", 1)

        val union = fixedFirstPlayer + fixedSecondPlayer

        return CompletedDataset(
                fixedFirstPlayer.map { it.entryToString(false) },
                fixedSecondPlayer.map { it.entryToString(false) },
                union.filter { it.proposition != "TRUE" }.map { it.entryToString(true) },
                "${divided.initial}."
        )
    }
    fun getAmbiguityHandling(n: Int): UnfinishedDataset {
        val entries = mutableListOf<DatasetEntry>()

        entries += "q -< s$n" withLevel 1
        entries += "~q -< p$n" withLevel 1


        entries += "~p${n / 2} -< r${n / 2}" withLevel 1


        entries += "s$n".asFact() withLevel 1
        entries += "p$n".asFact() withLevel 1

        entries += "r${n / 2}".asFact() withLevel 1

        for (i in (n - 1) downTo 1) {
            entries += "~s${i + 1} -< s$i" withLevel 1
            entries += DatasetEntry("~p${i + 1} -< p$i", 1)

            entries += DatasetEntry("s$i -< TRUE", 1)
            entries += DatasetEntry("p$i -< TRUE", 1)
        }

        for (i in (n / 2 - 1) downTo 1) {
            entries += DatasetEntry("~r${i + 1} -< r$i", 1)

            entries += DatasetEntry("r$i -< TRUE", 1)
        }

        return UnfinishedDataset(entries, "q" comesFrom "TRUE")
    }

    fun getFloatingConclusionsData(n: Int): UnfinishedDataset {
        val entries = mutableListOf<DatasetEntry>()

        for (i in 1..n) {
            entries += "q -< p$i" withLevel 1
            entries += "p$i -< TRUE" withLevel 1

            entries += "q -< ~p$i" withLevel 1
            entries += "~p$i -< TRUE" withLevel 1
        }

        return UnfinishedDataset(entries, "q" comesFrom "TRUE")
    }

    fun getTeamDefeatCascading(n: Int): UnfinishedDataset {
        val entries = mutableListOf<DatasetEntry>()

        val stack = ArrayDeque<String>(n * n)
        var globi = 0

        stack.addLast("p$globi")

        globi++

        for (i in 0 until n) {
            for (j in 1..(2.0).pow(i).toInt().times(2)) {
                val el: String = stack.pop()

                if (el.startsWith('~')) {
                    stack.addLast(el.removePrefix("~"))
                } else {
                    stack.addLast("~$el")
                }

                for (ik in 1..2) {
                    entries += "$el -< p$globi" withLevel i + 1
                    entries += "p$globi -< TRUE" withLevel i + 1

                    stack.addLast("~p$globi")

                    globi++
                }

            }

        }

        return UnfinishedDataset(entries, "p0" comesFrom "TRUE")
    }

    private inline infix fun String.comesFrom(to: String): String {
        return "$this -< $to"
    }

    private inline fun String.negate(): String =
            if (this.startsWith('~')) this.removePrefix("~") else "~$this"

    // Nu hebben alle argumenten nog dezelfde level
    fun getTeamDefeatDataset(n: Int): UnfinishedDataset {
        val entries = mutableListOf<DatasetEntry>()

        val stack = ArrayDeque<String>(n * n)
        var globi = 0

        stack.addLast("p0")
        globi++

        entries += "p0" comesFrom "p$globi" withLevel 1
        entries += "p0" comesFrom "TRUE" withLevel 1
        entries += "p0" comesFrom "p$globi" withLevel 1
        entries += "p0" comesFrom "TRUE" withLevel 1

        globi++

        for (j in 1..((2.0).pow(n).toInt() - 1)) {
            val el: String = stack.pop()

            for (ik in 1..2) {
                var rule = el.negate() comesFrom "p$globi"
                var fact = "p$globi" comesFrom "TRUE"

                entries += rule withLevel 1
                entries += fact withLevel 1

                globi++

                rule = "p${globi - 1}".negate() comesFrom "p$globi"
                fact = "p$globi" comesFrom "TRUE"

                entries += rule withLevel 1
                entries += fact withLevel 1

                stack.addLast("p$globi")

                globi++
            }

        }

        return UnfinishedDataset(entries, "p0" comesFrom "TRUE")
    }
}