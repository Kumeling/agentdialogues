/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava

import agentlib.AgentId
import agentlib.ConsoleLogger
import agentlib.ILogger
import agentlib.LogLevel
import agentlib.argumentation.NotImplementedException
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter

class FileLogger : ILogger {
    private var writer: PrintWriter? = null

    var logFile: File? = null

    private val consoleLogger = ConsoleLogger()

    override fun Log(id: AgentId, level: LogLevel, message: String, loggingEnabled: Boolean, currentLogLevel: LogLevel) {
        openFile()

        consoleLogger.Log(id, level, message, loggingEnabled, currentLogLevel)

        writer!!.println("$id\\$level: $message")
    }

    override fun ToStdIn(text: String) {
        openFile()

        text.split('\n')
                .map { "STDO\\I: $it" }
                .forEach { writer!!.println(it) }

        consoleLogger.ToStdIn(text)
    }

    private inline fun openFile() {
        if(writer == null) {
            writer = PrintWriter(BufferedWriter(FileWriter(logFile)))
        }
    }

    fun close() {
        writer!!.flush()
        writer!!.close()
    }

    override fun getStdIn(): String {
        throw NotImplementedException()
    }
}