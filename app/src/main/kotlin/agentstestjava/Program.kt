/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava


import agentlib.Logger
import agentlib.Platform
import agentlib.argumentation.StrictTurnTaking
import agentstestjava.blackargument.*
import agentstestjava.blackargument.ai.AiOpenTemplate
import agentstestjava.blackargument.ai.AssertTemplate
import agentstestjava.blackargument.ai.CloseTemplate
import agentstestjava.blackargument.ai.OpenAiInAiTemplate
import agentstestjava.blackargument.wi.OpenAiInWiTemplate
import agentstestjava.blackargument.wi.WiOpenTemplate
import agentstestjava.blackargument.withsupport.SupportLevelComparison
import agentstestjava.cli.*
import com.github.ajalt.clikt.core.subcommands
import net.sf.tweety.arg.delp.syntax.DefeasibleLogicProgram
import java.io.IOException
import java.util.*

object Program {
    // Define the roles used
    private val allPlayers = arrayOf(Roles.Opponent, Roles.Proponent)
    private val a = arrayOf(Roles.Proponent)
    private val b = arrayOf(Roles.Opponent)

    @Throws(IOException::class)
    @JvmStatic
    fun main(args: Array<String>) = Main().subcommands(Diag(), Ui(), Generate(), ReadLogs()).main(args)

    @JvmStatic
    fun initializePlatform(parser: DirectoryDelpParser, args: Diag): Platform<Roles, DelpLocution, DefeasibleLogicProgram> {
        // Supporting objects
        val protocol = BlackProtocol()
        val turnTakingRule = StrictTurnTaking<DelpLocution, Roles, DefeasibleLogicProgram>(ArrayList())

        val comparison = SupportLevelComparison()
        parser.delpRuleToLevel.forEach { rule, level -> comparison.add(rule, level) }

        val history = if(args.usebhtree) {
            BlackHistory(
                    comparison,
                    letTreeUseBlackAndHunter = true,
                    diacTreeGenerator = { parent, arg -> BHBetterDiacTree(parent, arg) }
            )
        } else {
            BlackHistory(
                    comparison,
                    letTreeUseBlackAndHunter = false,
                    diacTreeGenerator = { parent, arg -> BetterDiacTree(parent, arg) }
            )
        }

        val platform = Platform(protocol, history, turnTakingRule)

        // Start the dialogue with a kickoff, to guarantee the opening of a certain dialogue type
        history.openSubdialogue(DialogueTypes.KICKSTART_DIALOGUE, false)
        history.moveEqualsChecker = BlackMoveEqualCheck()

        // Add ai and wi from Hunter
        protocol.addTemplate(OpenAiInAiTemplate(allPlayers), DialogueTypes.BLACK_ARGUMENT_INQUIRY, OpenAiInAiTemplate.OpenAiInAI::class.java)
        protocol.addTemplate(AssertTemplate(allPlayers), DialogueTypes.BLACK_ARGUMENT_INQUIRY, AssertTemplate.Assert::class.java)
        protocol.addTemplate(CloseTemplate(allPlayers), DialogueTypes.BLACK_ARGUMENT_INQUIRY, CloseTemplate.Close::class.java)

        protocol.addTemplate(OpenAiInWiTemplate(allPlayers), DialogueTypes.BLACK_WARRANT_INQUIRY, OpenAiInWiTemplate.OpenAiInWi::class.java)
        protocol.addTemplate(AssertTemplate(allPlayers), DialogueTypes.BLACK_WARRANT_INQUIRY, AssertTemplate.Assert::class.java)
        protocol.addTemplate(CloseTemplate(allPlayers), DialogueTypes.BLACK_WARRANT_INQUIRY, CloseTemplate.Close::class.java)

        // Add the participating agents.
        val firstPlayer = DelpAgent(a[0], args.firststrategy, IgnoraAssertionAttitude(), IgnoreAcceptanceAttitude())
        val secondPlayer = DelpAgent(b[0], args.secondstrategy, IgnoraAssertionAttitude(), IgnoreAcceptanceAttitude())

        // Let them ignore their own attitudes (for any template that makes use of it).
        firstPlayer.isIgnoringAttitudes = true
        secondPlayer.isIgnoringAttitudes = true

        // Set the beginning knowledgebases
        firstPlayer.initialQuestion = parser.initialquestion
        firstPlayer.beliefBase = parser.firstplayers
        secondPlayer.beliefBase = parser.secondplayers

        // Assign the first player
        turnTakingRule.firstAgent = firstPlayer

        // Add the agents to the platform
        platform.addAgent(firstPlayer)
        platform.addAgent(secondPlayer)


        // Do last things for each specific dialogue
        if(args.dialoguetype == DialogueTypes.BLACK_ARGUMENT_INQUIRY) {
            // Assuming that an ai dialogue should be started first

            // Force the start of an ai dialogue
            protocol.addTemplate(AiOpenTemplate(allPlayers), DialogueTypes.KICKSTART_DIALOGUE, AiOpenTemplate.AiOpen::class.java)

        } else  {

            // Assuming a wi dialogue should be started first.

            // Force the start of a wi dialogue
            protocol.addTemplate(WiOpenTemplate(allPlayers), DialogueTypes.KICKSTART_DIALOGUE, WiOpenTemplate.WiOpen::class.java)

            if(!args.notree) {
                // After stopping, print the status of the tree:
                platform.addOnstopListener { p ->
                    val history1 = p.history as BlackHistory

                    Logger.stdin("\nGenerated tree:\n===============\n${history1.tree}\n")
                    Logger.stdin("Root argument:\n==============\n${history1.rootArgument}\n")
                    Logger.stdin("Status of root:\n===============\n${history1.tree?.marking}\n")

                    Logger.stdin("Number of rules:\n===============\n${parser.firstplayers.join(parser.secondplayers).size - 1}")

                    Logger.stdin("Number of arguments:\n===============\n${parser.firstplayers.join(parser.secondplayers).arguments.size - 1}")

                    Logger.stdin("Arguments in public space:\n===============\n${history1.argumentsMoved.arguments.size}")

                    if(args.dottree && history1.tree != null) {
                        //history1.constructTree()

                        val toJson = DiacTreeDot(history1.tree!!)

                        Logger.stdin("Commitment tree:\n===============\n")
                        Logger.stdin('\n')
                        Logger.stdin(toJson.getJson())

                        if(args.alsoFullTree) {

                            BlackHistory.callGetDefeaters(toJson.root,
                                    parser.firstplayers.join(parser.secondplayers),
                                    setOf(history1.rootArgument!!),
                                    history1.criterion,
                                    true,
                                    args.usebhtree)
                            Logger.stdin("Full tree:\n===============\n")
                            Logger.stdin(toJson.getJson())
                        }


                    }
                }
            }
        }

        return platform
    }

}


