package agentstestjava.blackargument

import net.sf.tweety.arg.delp.semantics.ComparisonCriterion
import net.sf.tweety.arg.delp.syntax.DefeasibleLogicProgram
import net.sf.tweety.arg.delp.syntax.DelpArgument
import net.sf.tweety.logics.commons.syntax.Predicate
import net.sf.tweety.logics.fol.syntax.FolAtom
import java.util.*

/**
 * This class implements a node of a dialectical tree from DeLP.
 *
 * @author Matthias Thimm
 *
 */
class BHBetterDiacTree(
        override val parent: IBetterDiacTree?,
        override var argument: DelpArgument)
    : IBetterDiacTree {
    override fun toString(): String {
        return "[$argument${if (children.isNotEmpty()) " - " else ""}${children.joinToString(transform = IBetterDiacTree::toString)}]"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as IBetterDiacTree?

        val queue = LinkedList<Pair<IBetterDiacTree, IBetterDiacTree>>()

        queue.add(this to that!!)

        while (queue.isNotEmpty()) {
            val item = queue.pop()

            if (item.first.argument != item.second.argument ||
                    item.first.children.size != item.second.children.size) {
                return false
            } else {
                val firstchild = item.first.children.iterator()
                val secondchild = item.second.children.iterator()

                while (firstchild.hasNext()) {
                    queue.add(firstchild.next() to secondchild.next())
                }
            }
        }

        return true
    }

    override fun hashCode(): Int {
        return Objects.hash(children, argument)
    }

    /**
     * The children of this node; size=0 indicates a leaf
     */
    override val children: MutableSet<IBetterDiacTree> = HashSet()

    /**
     * constructor; initializes this dialectical tree node as a root with given argument
     * @param argument a DeLP argument
     */
    constructor(argument: DelpArgument) : this(null, argument)

    /**
     * Computes the set of arguments which are defeaters for the argument in this tree node and returns
     * the corresponding dialectical tree nodes with these defeaters. For the computation of the defeaters
     * the whole argumentation line to this tree node is considered. As a side effect the computed tree nodes
     * are added as children of this node
     *
     * @param arguments a set of arguments
     * @param delp a defeasible logic program
     * @param comparisonCriterion a comparison criterion.
     * @return the set of defeater nodes of the argument in this node
     */
    override fun getNewDefeaters(arguments: Set<DelpArgument>,
                                 delp: DefeasibleLogicProgram,
                                 comparisonCriterion: ComparisonCriterion,
                                 addAsChild: Boolean,
                                 getArgumentsFromDlpToo: Boolean): Set<IBetterDiacTree> {

        // test parameters:
        val attackOpportunities = argument.getAttackOpportunities(delp)

        val argumentsOfChildren = children.map { it.argument }.toSet()

        // First do this for the arguments in the delp
        val attacksFromDelp = HashSet<DelpArgument>()
        for (lit in attackOpportunities) {
            attacksFromDelp.addAll(delp.arguments
                    .filter { argument ->
                        argument.conclusion != TruePredicate
                                && argument.conclusion == lit
                                && argument !in argumentsOfChildren
                    })
        }

        val defeatersFromDelp = attacksFromDelp
                .filter { attack -> isAcceptable(attack, delp, comparisonCriterion) }
                .toSet()

        // Then do this for [arguments]
        val attacksFromParameters = HashSet<DelpArgument>()
        for (lit in attackOpportunities) {
            attacksFromParameters.addAll(arguments
                    .filter { argument ->
                        argument.conclusion != TruePredicate
                                && argument.conclusion == lit
                                && argument !in argumentsOfChildren
                    })
        }
        //for each attacker check acceptability
        val defeatersFromParameters = attacksFromParameters
                .filter { attack -> isAcceptable(attack, delp, comparisonCriterion) }
                .toSet()


        //children.clear()


        // Always add attacks from DeLP, that are NOT from the parameters
        children.addAll(defeatersFromDelp
                .asSequence()
                .map { defeater -> BHBetterDiacTree(this, defeater) }
                .toSet())

        val newChildren = defeatersFromParameters
                .map { defeater -> BHBetterDiacTree(this, defeater) }
                .toSet()

        // Long story, we want to give back references that are actually present in [children]
        // If we were to just return newChildren, then we would possibly return instances which are in [children]
        // when looking at the contents, but which are actually reference-wise a different instance.
        val reallynewChildren = mutableSetOf<IBetterDiacTree>()

        for (child in newChildren) {
            if (child in this.children) {
                val doppelganger = this.children.first { it == child }

                reallynewChildren.add(doppelganger)
            } else {
                reallynewChildren.add(child)
            }
        }

        // Only add parameter attacks iff we are ordered to.
        if (addAsChild) {
            children.addAll(reallynewChildren)
        }

        return reallynewChildren
    }

    companion object {
        val TruePredicate = FolAtom(Predicate("TRUE"))
    }
}
