/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument

import agentlib.AgentId
import agentlib.Logger
import net.sf.tweety.arg.delp.parser.DelpParser
import net.sf.tweety.arg.delp.syntax.DefeasibleLogicProgram
import net.sf.tweety.arg.delp.syntax.DelpFact
import net.sf.tweety.arg.delp.syntax.DelpRule
import net.sf.tweety.commons.Formula
import net.sf.tweety.commons.Parser

import java.io.IOException
import java.text.MessageFormat
import java.util.HashMap
import java.util.Optional

class DirectoryDelpParser @Throws(IOException::class)
constructor(options: DelpParserOptions) {
    var firstplayers: DefeasibleLogicProgram
    var secondplayers: DefeasibleLogicProgram
    var initialquestion: DelpLocution
    private val parser: DelpParser = DelpParser()

    private val stringRuleToLevel = mutableMapOf<String, Int>()
    var delpRuleToLevel = mutableMapOf<DelpRule, Int>()

    val path = ""

    var levels = ""

    companion object {
        val id = AgentId("PARSE")
    }

    init {
        val firstText: String = options.firstPlayerText
                .replace('!', '~')
        val secondText: String = options.secondPlayerText
                .replace('!', '~')
        val initialText: String = options.initialText
                .replace('!', '~')
        val levelText: String = options.levelsText
                .replace('!', '~')

        // Let's parse first and second, and leave initial over to a subclass.
        firstplayers = parser.parseBeliefBase(firstText)
        secondplayers = parser.parseBeliefBase(secondText)
        initialquestion = parseInitial(initialText, parser)

        splitRulesAndLevels(levelText)
        convertRulesFromLevelsTxtToDelpRules()
    }

    fun splitRulesAndLevels(text: String) {
        levels = text

        text.split('\n')
                .filter { it.isNotEmpty() }
                .map {
                    val fields = it.split(';')

                    if (fields.size != 2) {
                        throw IllegalStateException("Syntax of file is wrong")
                    }

                    val rule = fields[0].trim()
                    val level = fields[1].trim().toInt()

                    stringRuleToLevel[rule] = level
                }
    }

    private fun convertRulesFromLevelsTxtToDelpRules() {
        val delpToInt = HashMap<DelpRule, Int>(stringRuleToLevel.size)
        val joined = firstplayers.join(secondplayers)
        val parser = DelpParser()

        for ((s, integer) in stringRuleToLevel) {
            val parsedDb = parser.parseBeliefBase(s)

            // [parser] gives an entire knowledge base which contains only one rule
            // We need that rule, therefore we use an iterator
            val parsedRule = parsedDb!!.iterator().next()

            // Try and find the matching rule
            val foundRule = joined.find {
                it == parsedRule
            }

            // If the matching rule exists, then put it into memory.
            // Else, issue a warning.
            if (foundRule == null) {
                Logger.w(id, MessageFormat.format("Rule {0} from levels.txt was ignored.", s))
            } else {
                delpToInt[foundRule] = integer
            }
        }

        delpRuleToLevel = delpToInt
    }

    fun reparseInitial(text: String) {
        initialquestion = parseInitial(text, DelpParser())
    }

    fun reparseFirstPlayer(text: String, parser: DelpParser) {
        firstplayers = parser.parseBeliefBase(text)
    }

    fun reparseSecondPlayer(text: String, parser: DelpParser) {
        secondplayers = parser.parseBeliefBase(text)
    }

    fun parseInitial(text: String, parser: Parser<DefeasibleLogicProgram, Formula>): DelpLocution {
        if ("-<" in text || "->" in text) {
            // Assume a rule
            // Parse and get the first (and only) rule.
            val rule = parser.parseBeliefBase(text).iterator().next()

            return DelpLocution(rule)
        } else {
            // Assume a proposition
            val rule = parser.parseFormula(text) as DelpFact

            return DelpLocution(rule.conclusion)
        }
    }
}