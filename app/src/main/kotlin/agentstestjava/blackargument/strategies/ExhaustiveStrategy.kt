/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.strategies

import agentlib.Logger
import agentlib.Platform
import agentlib.argumentation.*
import agentstestjava.blackargument.BlackHistory
import agentstestjava.blackargument.DelpLocution
import java.util.*

class ExhaustiveStrategy : IStrategy<DelpLocution> {
    override fun chooseMove(moves: Collection<IMove<DelpLocution>>,
                            history: IDialogueHistory<DelpLocution>,
                            otherAgent: ArgumentationAgent<DelpLocution, *, *>,
                            extra: ExtraInformation): Optional<IMove<DelpLocution>> {
        val movs = moves.sortedWith(comp)

        if(history is BlackHistory)
            Logger.d(Platform.PlatformId, "Current tree: ${history.tree}")


        return when {
            movs.isEmpty() -> Optional.empty()
            else           -> Optional.of(movs[0])
        }
    }

    companion object {
        private val comp = ExhaustiveMoveComparator
    }

}
