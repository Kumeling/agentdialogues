/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.strategies

import agentlib.Logger
import agentlib.Platform
import agentlib.argumentation.*
import agentstestjava.blackargument.BlackHistory
import agentstestjava.blackargument.DelpLocution
import agentstestjava.blackargument.ai.AssertTemplate
import java.util.*

/**
 * Exhaustive strategy, checks whether the argument would actually add something to the tree,
 * and if it does, that it changes the tree.
 */
class SmartExhaustiveStrategy : IStrategy<DelpLocution> {

    override fun chooseMove(moves: Collection<IMove<DelpLocution>>,
                            history: IDialogueHistory<DelpLocution>,
                            otherAgent: ArgumentationAgent<DelpLocution, *, *>,
                            extra: ExtraInformation): Optional<IMove<DelpLocution>> {
        val movs = moves.sortedWith(comp)
        val hist = history as BlackHistory

        Logger.d(Platform.PlatformId, "Current tree: ${hist.tree}")

        for (move in movs) {
            // Only check assert moves
            if(move is AssertTemplate.Assert &&
                    hist.rootArgument != null) { // Only check if there exists a rootargument
                val changesStatusWithNewDefs = hist.changesStatusOfRoot(move.propositions[0].argument!!)

                // If this argument adds something to the tree
                if(changesStatusWithNewDefs.changesTree) {
                    // And if it does NOT change the rootarguments status
                    if(!changesStatusWithNewDefs.changesStatusOfRoot) {
                        // Then skip it
                        continue
                    }
                }
            }

            // Otherwise, just return the move
            return Optional.of(move)
        }

        return Optional.empty()
    }


    companion object {
        private val comp = ExhaustiveMoveComparator
    }
}
