/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.strategies

import agentlib.argumentation.IMove
import agentstestjava.blackargument.DelpLocution
import agentstestjava.blackargument.ai.AiOpenTemplate
import agentstestjava.blackargument.ai.AssertTemplate
import agentstestjava.blackargument.ai.CloseTemplate
import agentstestjava.blackargument.ai.OpenAiInAiTemplate
import agentstestjava.blackargument.wi.OpenAiInWiTemplate
import agentstestjava.blackargument.wi.WiOpenTemplate
import java.util.*


object ExhaustiveMoveComparator : Comparator<IMove<DelpLocution>> {
    private val classes = LinkedList<Class<*>>()

    init {
        // The order these are declared in determine the order that the moves are sorted in,
        with(classes) {
            add(AssertTemplate.Assert::class.java)
            add(AiOpenTemplate.AiOpen::class.java)
            add(OpenAiInAiTemplate.OpenAiInAI::class.java)
            add(WiOpenTemplate.WiOpen::class.java)
            add(OpenAiInWiTemplate.OpenAiInWi::class.java)
            add(CloseTemplate.Close::class.java)
        }
    }

    override fun compare(t0: IMove<DelpLocution>, t1: IMove<DelpLocution>): Int {
        return classes.indexOf(t0.javaClass) - classes.indexOf(t1.javaClass)
    }
}