/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.strategies

import agentlib.argumentation.*
import agentstestjava.blackargument.DelpAgent
import agentstestjava.blackargument.DelpLocution
import agentstestjava.blackargument.ai.AssertTemplate
import agentstestjava.blackargument.join
import net.sf.tweety.arg.delp.syntax.DelpFact
import net.sf.tweety.logics.commons.syntax.Predicate
import net.sf.tweety.logics.fol.syntax.FolAtom
import net.sf.tweety.logics.fol.syntax.FolFormula
import java.util.*

/**
 * Exhaustive strategy, only now checks whether in commitment stores already an argument
 * is being made for the proposition for which we want to assert something.
 */
class LimitedCommitmentExhaustiveStrategy : IStrategy<DelpLocution> {

    override fun chooseMove(moves: Collection<IMove<DelpLocution>>,
                            history: IDialogueHistory<DelpLocution>,
                            otherAgent: ArgumentationAgent<DelpLocution, *, *>,
                            extra: ExtraInformation): Optional<IMove<DelpLocution>> {
        var movs = moves.sortedWith(comp)
        val delpextra = extra as DelpAgent.DelpExtraInformation
        val delpotheragent = otherAgent as DelpAgent

        val truepredicate = DelpFact(FolAtom(Predicate("TRUE")) as FolFormula?)

        val arguments = join(delpextra.commitmentStoreOfTurnTaker,
                delpotheragent.commitmentStore)
                .apply { this.add(truepredicate) }
                .arguments

        // The list should contain every conclusion we found an argument for.
        val litstocheckfor = arguments.map { it.conclusion }.toSet()

        for (move in movs) {
            if(move is AssertTemplate.Assert) {
                assert(move.propositions[0].isArgument)

                if(move.propositions[0].argument!!.conclusion in litstocheckfor) {
                    continue
                }
            }

            return Optional.of(move)
        }

        return Optional.empty()
    }

    companion object {
        private val comp = ExhaustiveMoveComparator
    }
}
