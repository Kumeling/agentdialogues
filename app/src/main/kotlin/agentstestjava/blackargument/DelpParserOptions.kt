package agentstestjava.blackargument

data class DelpParserOptions(
        var levelsText: String = "",
        var firstPlayerText: String = "",
        var secondPlayerText: String = "",
        var initialText: String = "")