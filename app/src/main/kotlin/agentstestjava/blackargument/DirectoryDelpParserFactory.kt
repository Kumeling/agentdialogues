package agentstestjava.blackargument

import java.io.File
import java.io.IOException
import java.nio.file.Files

object DirectoryDelpParserFactory {
    fun getFromFile(path: String): DirectoryDelpParser {
        val dir = File(path)

        if (!Files.exists(dir.toPath()) || !Files.isDirectory(dir.toPath())) {
            throw IllegalArgumentException("Path should be a directory")
        }

        val first = File(dir, "firstplayer.txt")
        val second = File(dir, "secondplayer.txt")
        val initial = File(dir, "initial.txt")
        val levels = File(dir, "levels.txt")

        if (!first.exists() || !second.exists() || !initial.exists() || !levels.exists()) {
            throw IllegalStateException("All files should exist and have the right names: firstplayer.txt, secondplayer.txt, initial.txt AND levels.txt")
        }

        val options = DelpParserOptions().apply {
            firstPlayerText = Files.readString(first.toPath())
            secondPlayerText = Files.readString(second.toPath())
            initialText = Files.readString(initial.toPath())
            levelsText = Files.readString(levels.toPath())
        }

        return DirectoryDelpParser(options)
    }
}