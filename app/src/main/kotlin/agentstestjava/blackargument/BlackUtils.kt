/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument

import agentlib.argumentation.IMove
import agentstestjava.blackargument.ai.CloseTemplate
import net.sf.tweety.arg.delp.syntax.DefeasibleLogicProgram
import net.sf.tweety.arg.delp.syntax.DelpRule
import net.sf.tweety.logics.fol.syntax.FolFormula

fun BlackHistory.matchedClose(): Boolean {
    if (length < 2) {
        return false
    }

    val lastMove: IMove<DelpLocution> = lastMove
    val singleToLast: IMove<DelpLocution> = this[length - 2]

    // Terminate upon matched-close AND if the last two close moves match
    return lastMove is CloseTemplate.Close && singleToLast is CloseTemplate.Close && moveEqualsChecker.moveEquals(lastMove, singleToLast)
}

fun join(vararg delps: DefeasibleLogicProgram) =
        delps.reduce { prog, prog2 -> prog.join(prog2) }

fun DefeasibleLogicProgram.join(right: DefeasibleLogicProgram) =
        DefeasibleLogicProgram().apply {
            addAll(this@join)
            addAll(right)
        }


fun DelpRule.getLiteralsOf(): Set<FolFormula> =
        HashSet(premise).apply{
            add(conclusion)
        }

fun DefeasibleLogicProgram.getDefeasibleDerivations() =
        arguments
        .map { it.conclusion }
        .toSet()

