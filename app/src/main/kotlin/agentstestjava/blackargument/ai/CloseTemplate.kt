/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.ai

import agentlib.AgentId
import agentlib.argumentation.ArgumentationAgent
import agentlib.argumentation.IMove
import agentlib.argumentation.Move
import agentlib.argumentation.movehistory.MoveHistory
import agentlib.argumentation.movehistory.MoveHistoryTemplate
import agentstestjava.Roles
import agentstestjava.blackargument.BlackHistory
import agentstestjava.blackargument.DelpLocution
import agentstestjava.blackargument.matchedClose
import net.sf.tweety.arg.delp.syntax.DefeasibleLogicProgram
import net.sf.tweety.logics.fol.syntax.FolFormula
import java.util.*

class CloseTemplate(applicableForRoles: Array<Roles>) : MoveHistoryTemplate<DelpLocution, Roles, DefeasibleLogicProgram, FolFormula>(applicableForRoles) {

    override fun isApplicableInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, addressedTo: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, roleOfTurnTaker: Roles): Boolean {
        return true
    }

    override fun getMovesInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, addressedTo: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, roleOfTurnTaker: Roles): List<IMove<DelpLocution>> {
        return listOf(Close(hisTurn.id, addressedTo.id, history.currentTopic))
    }

    override fun applyInternal(history: MoveHistory<DelpLocution, FolFormula>, hisTurn: ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram>, moveToApply: IMove<DelpLocution>) {
        // Close current subdialogue IF matched close

        // Here that thus means that the i

        if ((history as BlackHistory).matchedClose()) {
            history.closeCurrentDialogue()
        }
    }


    inner class Close(from: AgentId, addressedTo: AgentId, var topic: Set<FolFormula>) : Move<DelpLocution>(from, addressedTo, DelpLocution()) {

        override val name: String
            get() = "Close"

        override fun clone(): Move<DelpLocution> {
            return Close(from, addressedTo, topic)
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null || javaClass != other.javaClass) return false
            // Do NOT compare closed upon the sender/receiver.
            val close = other as Close?

            for (formula in this.topic) {
                if (!close!!.topic.contains(formula)) {
                    return false
                }
            }
            return true
        }

        override fun hashCode(): Int {

            return Objects.hash(super.hashCode(), topic)
        }

        override fun toString(): String {
            return name + " " + this.topic
        }
    }
}
