/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.cli

import agentstestjava.DatasetGenerator
import agentstestjava.DatasetGenerator.dividedDatasetToDataset
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.default
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.int
import com.github.ajalt.clikt.parameters.types.path
import java.nio.file.Files
import java.nio.file.Path

class Generate : CliktCommand() {
    val nameofdataset: String by argument(help = "The name of the dataset to generate")
            .choice("floating", "teamdefeat", "cascading", "ambiguity")

    val n: Int by argument(help = "The size of the to-be-generated data set.")
            .int()

    val directory: Path by argument(help = "The directory to place the data set in. It WILL be overwritten!")
            .path(exists = false, fileOkay = false, folderOkay = true)

    val datasetdivider: DatasetGenerator.DatasetDivider by option(help = "How to divide the knowledge over the agents.")
            .choice("random" to DatasetGenerator.RandomDivider(),
                    "default" to DatasetGenerator.FirstPlayerFactsDivider())
            .default(DatasetGenerator.FirstPlayerFactsDivider())

    override fun run() {
        //TermUi.confirm("Do you want to override the files in that folder?", default = false, abort = true)

        val unfinished = when (nameofdataset) {
            "floating" -> DatasetGenerator.getFloatingConclusionsData(n)
            "teamdefeat" -> DatasetGenerator.getTeamDefeatDataset(n)
            "cascading" -> DatasetGenerator.getTeamDefeatCascading(n)
            "ambiguity" -> DatasetGenerator.getAmbiguityHandling(n)
            else -> DatasetGenerator.UnfinishedDataset(emptyList(), "")
        }

        val divided = datasetdivider.divideDatasetEntries(unfinished)
        val dataset = dividedDatasetToDataset(divided)

        assert(dataset.firstPlayer.size > 1)

        if(!Files.exists(directory)) {
            println(directory.toAbsolutePath())
            Files.createDirectories(directory)
        }

        Files.write(directory.resolve("firstplayer.txt"), dataset.firstPlayer)
        Files.write(directory.resolve("secondplayer.txt"), dataset.secondPlayer)
        Files.write(directory.resolve("initial.txt"), listOf(dataset.initial))
        Files.write(directory.resolve("levels.txt"), dataset.levels)
    }

}