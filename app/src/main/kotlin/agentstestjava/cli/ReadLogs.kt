/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.cli

import agentlib.AgentId
import agentlib.Logger
import agentstestjava.convertHistory
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.types.path
import guru.nidi.graphviz.engine.Format
import guru.nidi.graphviz.engine.Graphviz
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.io.FilenameUtils
import java.io.File
import java.io.StringWriter
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class ReadLogs : CliktCommand(name = "readlogs", help = "Helps to output a csv containing some data from ran data sets.") {
    val pathToDatasets by argument().path(exists = true, fileOkay = false, readable = true, writable = false)

    val id = AgentId("READ")

    override fun run() {

        val rootargregex = """Root argument:\n=+\n(.+)\n""".toRegex()
        val statusofrootregex = """Status of root:\n=+\n(.+)\n""".toRegex()
        val numberofrulesregex = """Number of rules:\n=+\n(.+)\n""".toRegex()
        val numberofargumentsregex = """Number of arguments:\n=+\n(.+)\n""".toRegex()
        val argumentsinpublicspaceregex = """Arguments in public space:\n=+\n(.+)\n""".toRegex()
        val commitmenttreeregex = """Commitment tree:\n=+\n+([\w \{\n\"\<\!\-\.\,\}\>\;]+})""".toRegex()
        val fulltreeregex = """Full tree:\n=+\n+([\w \{\n\"\<\!\-\.\,\}\>\;]+})""".toRegex()
        val historyregex = """Dumping history:\n=+([\w \{\n"\<\!\-\.\,\}\>\;\:\[\]]+)""".toRegex()

        val directories = pathToDatasets.toFile().walkTopDown()
                .filter { it.isDirectory }
                .filter { it != pathToDatasets.toFile() }

        val writer = StringWriter()

        val tsv = CSVPrinter(writer, CSVFormat.TDF.withHeader(
                "Dataset",
                "N",
                "Run",  // The run signifies that this was a random run.
                "Strategy",
                "BHTree",
                "RootArg",
                "StatusOfRoot",
                "NumberOfRules",
                "NumberOfArgs",
                "Argsinpublicspace",
                "commitmenttree",
                "fulltree",
                "sizeOfHistory",
                "sizeOfTree"))

        for (directory in directories) {
            Logger.d(id, "Now in ${directory.name}")

            val files = directory.listFiles { _,
                                              name ->
                name.startsWith("log")
                        && name.endsWith(".txt")
            }

            // Skip directories whose name is not of the format '<datasetname>_<size>'
            if ('_' !in directory.name) {
                continue
            }

            // A random dataset's directory has got the name '<datasetname>_<size>_<run>'
            val isRandomDataset = directory.name.split('_').size > 2

            val dataset = directory.name.split('_')
            val datasetName = dataset[0]
            val n = dataset[1].toInt()
            val run = if (isRandomDataset) dataset[2].toInt() else 0

            for (file in files) {
                Logger.d(id, "Now converting: ${file.name}")

                val items = file.nameWithoutExtension.split('_')
                val strategyUsed = items[1]
                val bhTreeUsed = items[3]

                val text = file.readLines()
                        .asSequence()
                        .filter { it.startsWith("STDO\\I: ") }
                        .map { it.removePrefix("STDO\\I: ") }
                        .reduce { acc, s -> acc + "\n" + s }

                val rootarg = rootargregex.find(text)!!.groupValues[1]
                val statusofroot = statusofrootregex.find(text)!!.groupValues[1]
                val numberofrules = numberofrulesregex.find(text)!!.groupValues[1].toInt()
                val numberofarguments = numberofargumentsregex.find(text)!!.groupValues[1].toInt()
                val argumentsinpublicspace = argumentsinpublicspaceregex.find(text)!!.groupValues[1].toInt()
                val commitmenttree = commitmenttreeregex.find(text)!!.groupValues[1]
                val fulltree = fulltreeregex.find(text)!!.groupValues[1]
                val history = historyregex.find(text)!!.groupValues[1]
                val sizeOfHistory = history.split('\n').size
                // Size of tree measured in the number of arguments present in the tree
                // The number of arguments present is the number of attacks
                // Plus 1 to compensate for the root argument that attacks noone
                val sizeOfTree: Int = fulltree.split('\n').size - 2 + 1

                // Render the tree from this strategy to SVG
                val svgFile = File(FilenameUtils.removeExtension(file.absolutePath) + ".svg")
                val pngFile = File(FilenameUtils.removeExtension(file.absolutePath) + ".png")
                val histFile = File(FilenameUtils.removeExtension(file.absolutePath) + ".latex")

                Logger.d(id, "Writing commitment tree to svg")
                Graphviz.fromString(commitmenttree)
                        .render(Format.SVG)
                        .toFile(svgFile)

                Logger.d(id, "Writing commitment tree to PNG")
                Graphviz.fromString(commitmenttree)
                        .render(Format.PNG)
                        .toFile(pngFile)

                Logger.d(id, "Writing history to LaTeX")
                Files.write(histFile.toPath(), convertHistory(history.split('\n')))

                Logger.d(id, "Writing experiment to CSV")
                tsv.printRecord(
                        datasetName,
                        n,
                        run,
                        strategyUsed,
                        bhTreeUsed,
                        rootarg,
                        statusofroot,
                        numberofrules,
                        numberofarguments,
                        argumentsinpublicspace,
                        commitmenttree,
                        fulltree,
                        sizeOfHistory,
                        sizeOfTree)
            }
        }

        tsv.close(true)

        Logger.i(id, "Done")
        pathToDatasets.resolve(".\\result.tsv")
                .toFile()
                .writeText(writer.toString())
    }
}