/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.cli

import agentlib.*
import agentlib.argumentation.IStrategy
import agentlib.argumentation.movehistory.MoveHistory
import agentstestjava.AskStrategy
import agentstestjava.FileLogger
import agentstestjava.Program
import agentstestjava.blackargument.DelpLocution
import agentstestjava.blackargument.DirectoryDelpParser
import agentstestjava.blackargument.DirectoryDelpParserFactory
import agentstestjava.blackargument.StringLogger
import agentstestjava.blackargument.strategies.ExhaustiveStrategy
import agentstestjava.blackargument.strategies.LimitedCommitmentExhaustiveStrategy
import agentstestjava.blackargument.strategies.LimitedDialogueExhaustiveStrategy
import agentstestjava.blackargument.strategies.SmartExhaustiveStrategy
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.counted
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.path
import java.io.File
import java.nio.file.Path

class Diag : CliktCommand() {
    val dialoguetype: String by argument(help = "The dialogue type to start with.")
    val pathtoknowledgebases: Path by argument(help = "The path to the knowledge bases to be used. I need a directory as an argument, containing firstplayer.txt, secondplayer.txt and initial.txt")
            .path(exists = true, readable = true, fileOkay = false)

    val doNotRun by option(help = "Do not run anything", hidden = true)
            .flag()

    val firststrategy: IStrategy<DelpLocution> by option(
            "-f",
            help = "The strategy to be used by the proponent."
    )
            .choice("ask" to AskStrategy(),
                    "exhaust" to ExhaustiveStrategy(),
                    "limitedcom" to LimitedCommitmentExhaustiveStrategy(),
                    "limiteddiag" to LimitedDialogueExhaustiveStrategy(),
                    "smart" to SmartExhaustiveStrategy())
            .default(ExhaustiveStrategy())

    val secondstrategy: IStrategy<DelpLocution> by option(
            "-s",
            help = "The strategy to be used by the opponent."
    )
            .choice("ask" to AskStrategy(),
                    "exhaust" to ExhaustiveStrategy(),
                    "limitedcom" to LimitedCommitmentExhaustiveStrategy(),
                    "limiteddiag" to LimitedDialogueExhaustiveStrategy(),
                    "smart" to SmartExhaustiveStrategy())
            .default(ExhaustiveStrategy())

    val logger: ILogger by option(
            "-l",
            help = "The logger to use with the dialogue system")
            .choice("stdin" to ConsoleLogger(),
                    "string" to StringLogger(),
                    "file" to FileLogger())
            .default(ConsoleLogger())

    val verbosity: Int by option(
            "-v",
            help = "Increase the logging level, if logging is enabled."
    ).counted()

    val nologging: Boolean by option(help = "Whether to turn logging completely off. Not recommended").flag()

    val nohistory: Boolean by option(help = "Whether to print the history of the dialogue.").flag()

    val nodb: Boolean by option(help = "Whether to NOT print the knowledge bases of the players.").flag()

    val notree: Boolean by option(help = "Whether to print the tree after a wi dialogue").flag()

    val usebhtree: Boolean by option(help = "Whether to use the Black and Hunter definition of a tree").flag()

    val dottree: Boolean by option(help = "Whether to also output the tree as a dot tree.").flag()

    val alsoFullTree: Boolean by option(
            help = "Whether to, besides the commitment tree, also show the full tree as dot tree"
    ).flag()

    override fun run() {
        if (doNotRun) {
            return
        }

        Logger.sink = logger

        if (logger is FileLogger) {
            val fileName = "log_${firststrategy::class.simpleName}_${secondstrategy::class.simpleName}_$usebhtree.txt"

            (logger as FileLogger).logFile = File(pathtoknowledgebases.toFile(), fileName)
        }

        Logger.enabled = !nologging

        Logger.logLevel = when (verbosity) {
            0, 1, 2 -> LogLevel.W
            3 -> LogLevel.I
            else -> LogLevel.D
        }
        val parser = DirectoryDelpParserFactory.getFromFile(pathtoknowledgebases.toAbsolutePath().toString())


        if (!nodb) {
            Logger.stdin(parser.firstplayers)
            Logger.stdin(parser.secondplayers)
            Logger.stdin(parser.initialquestion)
        }

        val p: Platform<*, *, *>

        p = Program.initializePlatform(parser, this)

        if (!nohistory) {
            p.addOnstopListener { p2 -> printHistory(p2.history as MoveHistory<*, *>) }
        }

        p.reset()
        p.run()

        if (logger is FileLogger) {
            (logger as FileLogger).close()
        }

    }

    fun printHistory(history: MoveHistory<*, *>) {

        Logger.stdin("Dumping history:\n================")

        for (i in 0 until history.length) {
            val move = history[i]
            Logger.stdin("${move.from}: $move")
        }

    }
}
