/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.gui

import agentlib.Logger
import agentlib.Platform
import agentstestjava.DiacTreeDot
import agentstestjava.Program
import agentstestjava.blackargument.BlackHistory
import agentstestjava.blackargument.DirectoryDelpParser
import agentstestjava.blackargument.DirectoryDelpParserFactory
import agentstestjava.blackargument.StringLogger
import agentstestjava.cli.Diag
import guru.nidi.graphviz.engine.Format
import guru.nidi.graphviz.engine.Graphviz
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.ToggleGroup
import javafx.scene.image.Image
import javafx.stage.DirectoryChooser
import net.sf.tweety.arg.delp.parser.DelpParser
import tornadofx.Controller
import java.io.File
import javafx.embed.swing.SwingFXUtils

class MainController : Controller() {
    var history = SimpleStringProperty("")
    var programlog = SimpleStringProperty("")
    var dbOfFirst = SimpleStringProperty("")
    var dbOfSecond = SimpleStringProperty("")
    var initialQuestion = SimpleStringProperty("")
    var levels = SimpleStringProperty("")

    var parser: DirectoryDelpParser? = null

    var legalMoves = SimpleStringProperty("")

    val firstPersonToggleGroup = ToggleGroup()
    val secondPersonToggleGroup = ToggleGroup()

    var currentTree: SimpleObjectProperty<Image?> = SimpleObjectProperty(null)

    init {
        Logger.sink = StringLogger()


        val pathToDataset : String? = this.app.parameters.raw[0]

        if (pathToDataset != null && pathToDataset != "null") {
            parser = DirectoryDelpParserFactory.getFromFile(pathToDataset)
        }

        reloadThings()
    }

    private fun reloadThings() {
        dbOfFirst.set(parser?.firstplayers.toString().replace('!', '~'))
        dbOfSecond.set(parser?.secondplayers.toString().replace('!', '~'))
        initialQuestion.set(parser?.initialquestion.toString().replace('!', '~'))
        levels.set(parser?.levels?.replace('!', '~'))
    }

    fun reParse() {
        parser?.splitRulesAndLevels(levels.get())
        parser?.reparseInitial(initialQuestion.get())
        parser?.reparseFirstPlayer(dbOfFirst.get(), DelpParser())
        parser?.reparseSecondPlayer(dbOfSecond.get(), DelpParser())

        reloadThings()
    }

    fun run() {
        platform?.run()

        updateObservableValues()
    }

    var platform: Platform<*, *, *>? = null

    private fun updateObservableValues() {
        if (platform == null) {
            return
        }

        if (Logger.sink is StringLogger) {
            programlog.set((Logger.sink as StringLogger).sb.toString())
        } else {
            programlog.set(Logger.getStdin())
        }

        if (platform!!.isDialogueStopped) {
            programlog.set("${programlog.get()}\n${Logger.getStdin()}")
        }

        if (platform?.history is BlackHistory) {
            val blackhist = platform?.history as BlackHistory

            history.set(moveHistoryToString(blackhist))

            if (blackhist.tree != null) {
                val dottreegen = DiacTreeDot(blackhist.tree!!)

                val dottree = dottreegen.getJson()

                val image = Graphviz.fromString(dottree).render(Format.PNG).toImage()

                currentTree.set(SwingFXUtils.toFXImage(image, null))
            }
        }
    }

    fun moveHistoryToString(hist: BlackHistory) = hist.moves.joinToString(separator = "\n") { "${it.from}: $it" }

    fun initializePlatform() {

        val command = Diag()

        val firststrat = convertStrategy(firstPersonToggleGroup.selectedToggle.properties["tornadofx.toggleGroupValue"] as String)
        val secondstrat = convertStrategy(secondPersonToggleGroup.selectedToggle.properties["tornadofx.toggleGroupValue"] as String)

        val pathToDataset = parser!!.path

        var cmdline = "--do-not-run"

        if (firststrat.usebhTree || secondstrat.usebhTree) {
            cmdline += " --usebhtree"
        }

        cmdline += " -f ${firststrat.strategy} -s ${secondstrat.strategy}"

        cmdline += " black_wi $pathToDataset"

        command.parse(cmdline.split(' '))

        platform = Program.initializePlatform(parser!!, command)
        Logger.sink = StringLogger()
    }

    data class StrategyRequested(val strategy: String, val usebhTree: Boolean)

    fun convertStrategy(strat: String): StrategyRequested =
            when (strat) {
                "smartorig" -> StrategyRequested("smart", true)
                "smartdiag" -> StrategyRequested("smart", false)
                else -> StrategyRequested(strat, true)
            }

    fun step() {
        platform?.step()

        showLegalMoves()

        updateObservableValues()
    }

    fun chooseDifferentDirectory() {
        val chooser = DirectoryChooser()

        with(chooser) {
            initialDirectory = File(System.getProperty("user.dir"))
            title = "Choose a directory to load the kb's from."
        }

        val dirChosen = chooser.showDialog(null) ?: return

        parser = DirectoryDelpParserFactory.getFromFile(dirChosen.absolutePath.toString())

        reloadThings()
    }

    fun showLegalMoves() {
        legalMoves.set(platform?.getLegalMoves()
                ?.joinToString(separator = "\n") { it.toString() })
    }

}