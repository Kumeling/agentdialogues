/*
 *  Copyright (C) 2018 Wouter Kumeling
 *
 *  This file is part of Agent Dialogues
 *
 *  Agent Dialogues is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This source code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this source code; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package agentstestjava.blackargument.wi;

import agentlib.AgentId;
import agentlib.argumentation.ArgumentationAgent;
import agentlib.argumentation.IMove;
import agentlib.argumentation.Move;
import agentlib.argumentation.movehistory.MoveHistory;
import agentlib.argumentation.movehistory.MoveHistoryTemplate;
import agentstestjava.DialogueTypes;
import agentstestjava.Roles;
import agentstestjava.blackargument.BlackHistory;
import agentstestjava.blackargument.BlackUtilsKt;
import agentstestjava.blackargument.DelpAgent;
import agentstestjava.blackargument.DelpLocution;
import net.sf.tweety.arg.delp.syntax.DefeasibleLogicProgram;
import net.sf.tweety.arg.delp.syntax.DelpFact;
import net.sf.tweety.arg.delp.syntax.DelpRule;
import net.sf.tweety.logics.commons.syntax.Predicate;
import net.sf.tweety.logics.fol.syntax.FolAtom;
import net.sf.tweety.logics.fol.syntax.FolFormula;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class OpenAiInWiTemplate extends MoveHistoryTemplate<DelpLocution, Roles, DefeasibleLogicProgram, FolFormula> {
    public OpenAiInWiTemplate(Roles[] applicableForRoles) {
        super(applicableForRoles);
    }

    @Override
    protected boolean isApplicableInternal(MoveHistory<DelpLocution, FolFormula> history, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> hisTurn, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> addressedTo, Roles roleOfTurnTaker) {
        return history.getLength() > 0;
    }

    @Override
    protected List<IMove<DelpLocution>> getMovesInternal(MoveHistory<DelpLocution, FolFormula> history, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> hisTurn, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> addressedTo, Roles roleOfTurnTaker) {
        BlackHistory blackHistory = (BlackHistory) history;

        List<DelpRule> list = new ArrayList<>();
        DefeasibleLogicProgram combined = ((DelpAgent) hisTurn).getKnowledge(addressedTo.getCommitmentStore());
        DefeasibleLogicProgram commstores = BlackUtilsKt.join(hisTurn.getCommitmentStore(), addressedTo.getCommitmentStore());

        // Add TRUE to commstores, to have proper reasoning.
        commstores.add(new DelpFact(new FolAtom(new Predicate("TRUE"))));

        Set<FolFormula> derivations = BlackUtilsKt.getDefeasibleDerivations(commstores);

        Predicate p = new Predicate("TRUE");

        combined.stream()
                .filter(r -> !r.isFact())
                .filter(r -> !r.getPremise().contains(new FolAtom(p)))
                .forEach(r -> {
                    if (blackHistory.getRootArgument() == null && history.getCurrentTopic().contains(r.getConclusion())
                            || derivations.contains(r.getConclusion().complement())) {
                        if (r.getPremise().size() > 0) {
                            list.add(r);
                        }
                    }
                });

        // TODO: Filter NOT depending on player!
        return list.stream()
                .map(r1 -> new OpenAiInWi(hisTurn.getId(), addressedTo.getId(), new DelpLocution(r1)))
                .filter(r1 -> !history.contains(r1))
                .collect(Collectors.toList());
    }

    @Override
    protected void applyInternal(MoveHistory<DelpLocution, FolFormula> history, ArgumentationAgent<DelpLocution, Roles, DefeasibleLogicProgram> hisTurn, IMove<DelpLocution> moveToApply) {
        // Open subdialogue with specific topic
        history.openSubdialogue(DialogueTypes.BLACK_ARGUMENT_INQUIRY, false);

        // Fill the topic with the argument's things.
        for (FolFormula formula : BlackUtilsKt.getLiteralsOf(moveToApply.getPropositions().get(0).getRule())) {
            if (!formula.toString().equals("TRUE")) {
                history.addToCurrentTopic(formula);
            }
        }

    }

    public class OpenAiInWi extends Move<DelpLocution> {

        public OpenAiInWi(AgentId from, AgentId addressedTo, DelpLocution proposition) {
            super(from, addressedTo, proposition);

            if (!proposition.isInferenceRule()) {
                throw new IllegalArgumentException();
            }
        }

        @Override
        public Move<DelpLocution> clone() {
            return new OpenAiInWi(getFrom(), getAddressedTo(), getPropositions().get(0));
        }

        @Override
        public String getName() {
            return "OpenAiInWi";
        }

    }
}
