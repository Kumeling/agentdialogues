#Requires -Version 6.0

$oldpwd = $PWD
$distdir = Join-Path $PWD -ChildPath "\app\build\install\app"

if ($isWindows) {
    $appbin = "app.bat"
}
else {
    $appbin = "app"
}

if ($isWindows) {
    $gradle = "gradlew.bat"
}
else {
    $gradle = "gradlew"
}

$appbindir = Join-Path $distdir "bin" $appbin

$randomtestsdir = Join-Path $distdir "randomTests"
$normaltestsdir = Join-Path $distdir "normalTests"
$gradledir = Join-Path $oldpwd $gradle

function constructDiagCmdLine {
    param(
        [Parameter(Mandatory=$true)]
        [String]$strategyname,
        [Parameter(Mandatory=$true)]
        [String]$datasetdir
    )
    # USe the BH tree standard. Only the smart dialogue strategy uses the tree
    # based on the dialogue.
    $useBHTree = $true
    $stratForCmdLine = ""
            
    switch ($strategyname) {
        "smartdiag" { 
            $stratForCmdLine = "smart"
            $useBHTree = $false
            break;
        }
        "smartorig" {
            $stratForCmdLine = "smart"
            break;
        }
        Default {
            $stratForCmdLine = $strategyname
        }
    }
            
    $cmdline = @("diag" , "-s", $stratForCmdLine, "-f", $stratForCmdLine, "--dottree", "--also-full-tree", "-l", "file", "-vvvvvvv")
            
    if ($useBHTree) {
        $cmdline += "--usebhtree"
    }
            
    $cmdline += @("black_wi", $datasetdir)

    return $cmdline
}

function doInstallDist {
    Remove-Item -Recurse "$distdir"

    & $gradledir installDist
}

function isCompleted {
    param (
        [String]$datasetdir,
        [String]$strategy
    )
    $logFilename = toLogFileName $strategy
    $dirtotest = Join-Path "$datasetdir" (getCompletedFileName $logFilename)

    return Test-Path -Path "$dirtotest" -PathType Leaf
}

function markCompleted {
    param (
        [String]$datasetdir,
        [String]$strategy
    )
    $logFilename = toLogFileName $strategy
    $dirtotest = Join-Path "$datasetdir" (getCompletedFileName $logFilename)

    if(isCompleted $datasetdir $strategy) {
        throw [System.InvalidOperationException] "Completed file ($dirtotest) already exists."
    }

    New-Item -Path $dirtotest -ItemType File 
}

function getCompletedFileName {
    param (
        [String]$logFileName
    )
    return ".$logFileName`_completed"
}

function toLogFileName {
    param (
        [String]$strategy
    )
    
    switch ($strategy) {
        "smartdiag" { "log_SmartExhaustiveStrategy_SmartExhaustiveStrategy_false" }
        "smartorig" { "log_SmartExhaustiveStrategy_SmartExhaustiveStrategy_true" }
        "limitedcom" { "log_LimitedCommitmentExhaustiveStrategy_LimitedCommitmentExhaustiveStrategy_true" }
        "limiteddiag" { "log_LimitedDialogueExhaustiveStrategy_LimitedDialogueExhaustiveStrategy_true" }
        "exhaust" { "log_ExhaustiveStrategy_ExhaustiveStrategy_true" }
        Default { throw [System.ArgumentOutOfRangeException] "Strategy $strategy is not known." }
    }
}